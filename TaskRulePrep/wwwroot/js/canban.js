﻿const drag = (event) => {
    event.dataTransfer.setData("text/plain", event.target.id);
}

const allowDrop = (ev) => {
    ev.preventDefault();
    if (hasClass(ev.target, "dropzone")) {
        addClass(ev.target, "droppable");
    }
}

const clearDrop = (ev) => {
    removeClass(ev.target, "droppable");
}

const drop = (event) => {
    event.preventDefault();
    const data = event.dataTransfer.getData("text/plain");
    const element = document.querySelector(`#${data}`);
    try {
        // remove the spacer content from dropzone
        event.target.removeChild(event.target.firstChild);
        // add the draggable content
        event.target.appendChild(element);
        // remove the dropzone parent
        unwrap(event.target);
    } catch (error) {
        console.warn("can't move the item to the same place")
    }
    updateDropzones();
}

const updateDropzones = () => {
    /* after dropping, refresh the drop target areas
      so there is a dropzone after each item
      using jQuery here for simplicity */

    var dz = $('<div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>');

    // delete old dropzones
    $('.dropzone').remove();

    // insert new dropdzone after each item   
    dz.insertAfter('.card.draggable');

    // insert new dropzone in any empty swimlanes
    $(".items:not(:has(.card.draggable))").append(dz);
};

// helpers
function hasClass(target, className) {
    return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
}

function addClass(ele, cls) {
    if (!hasClass(ele, cls)) ele.className += " " + cls;
}

function removeClass(ele, cls) {
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
}

function unwrap(node) {
    node.replaceWith(...node.childNodes);
}
var KanbanTest = new jKanban({
    element: '#myKanban',
    gutter: '10px',
    click: function (el) {
        alert(el.innerHTML);
        alert(el.dataset.eid)
    },
    boards: [
        {
            'id': '_todo',
            'title': 'To Do (drag me)',
            'class': 'info',
            'item': [
                {
                    'id': 'task-1',
                    'title': 'Try drag me',
                },
                {
                    'id': 'task-2',
                    'title': 'Click me!!',
                }
            ]
        },
        {
            'id': '_working',
            'title': 'Working',
            'class': 'warning',
            'item': [
                {
                    'title': 'Do Something!',
                },
                {
                    'title': 'Run?',
                }
            ]
        },
        {
            'id': '_done',
            'dragTo': ['_working'],
            'title': 'Done (Drag only in Working)',
            'class': 'success',
            'item': [
                {
                    'title': 'All right',
                },
                {
                    'title': 'Ok!',
                }
            ]
        }
    ]
});

var toDoButton = document.getElementById('addToDo');
toDoButton.addEventListener('click', function () {
    KanbanTest.addElement(
        '_todo',
        {
            'title': 'Test Add',
        }
    );
});

var addBoardDefault = document.getElementById('addDefault');
addBoardDefault.addEventListener('click', function () {
    KanbanTest.addBoards(
        [{
            'id': '_default',
            'title': 'Default (Can\'t drop in Done)',
            'dragTo': ['_todo', '_working'],
            'class': 'error',
            'item': [
                {
                    'title': 'Default Item',
                },
                {
                    'title': 'Default Item 2',
                },
                {
                    'title': 'Default Item 3',
                }
            ]
        }]
    )
});

var removeBoard = document.getElementById('removeBoard');
removeBoard.addEventListener('click', function () {
    KanbanTest.removeBoard('_done');
});
