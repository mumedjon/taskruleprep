﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;

namespace TaskRulePrep.Pages
{
    public class PersonModel : PageModel
    {
        public string Mess { get; set; }

        public void OnGet()
        {
            Mess = "Enter data";
        }
        [BindProperty]
        public string namee { get; set; }
        [BindProperty]
        public int age { get; set; }
        public void OnPost()
        {
            Mess = $"Name {namee} Age {age}";

        }
    }
}